﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialsHolder : MonoBehaviour
{
    public Material[] materials;

    public GameObject sphere;

    int i = 0;
    
    public void SwapMaterial()
    {
        gameObject.GetComponent<Renderer>().material = materials[i];
        i++;
        if(i == materials.Length)
        {
            i = 0;
        }
    }
}
